package com.kazakimaru.ch04t2

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.kazakimaru.ch04t2.databinding.FragmentDialogBinding

class DialogFragment : Fragment() {

    private var _binding: FragmentDialogBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showAlertDialogStandard()
        showAlertDialogWithAction()
        showtAlertDialogCustom()
        showDialogFragment()
    }

    private fun showDialogFragment() {
        binding.btnShowDialogfragment.setOnClickListener {
            val dialogFragment = MyDialogFragment()
            dialogFragment.show(requireActivity().supportFragmentManager, null)
        }
    }

    private fun showtAlertDialogCustom() {
        // Menghubungkan layout
        val customLayout = LayoutInflater.from(requireContext()).inflate(R.layout.layout_dialogcustom, null, false)

        // Mmebuat builder AlertDialog
        val dialogBuilder = AlertDialog.Builder(requireContext())

        // Memanggil textview dan button dari custom layout
        val buttonOne = customLayout.findViewById<Button>(R.id.btn_one)
        val title = customLayout.findViewById<TextView>(R.id.tv_title)
        
        // Mengubah text dari textview dan button
        title.text = "Telah diubah"
        buttonOne.text = "Dismiss"

        // Mengubah layout AlertDialog menggunakan custom layout
        dialogBuilder.setView(customLayout)

        // Membuat AlertDialog baru dari builder yang sudah di custom layout
        val dialog = dialogBuilder.create()

        // Action pada button
        buttonOne.setOnClickListener {
            dialog.dismiss()
        }

        binding.btnShowAlertdialogcustom.setOnClickListener {
            dialog.show()
        }

    }

    private fun showAlertDialogWithAction() {
        binding.btnShowAlertdialogaction.setOnClickListener {
            val dialog = AlertDialog.Builder(requireContext())
            dialog.setTitle("Ini title dialog")
            dialog.setMessage("Ini deskripsi dialog")
            dialog.setCancelable(true)
            dialog.setPositiveButton("Positive") { dialogInterface, pl ->
                Toast.makeText(requireContext(), "Positive Button Clicked", Toast.LENGTH_SHORT).show()
            }
            dialog.setNeutralButton("Neutral") { dialogInterface, pl ->
                Toast.makeText(requireContext(), "Neutral Button Clicked", Toast.LENGTH_SHORT).show()
            }
            dialog.setNegativeButton("Negative") { dialogInterface, pl ->
                Toast.makeText(requireContext(), "Negative Button Clicked", Toast.LENGTH_SHORT).show()
            }
            dialog.show()
        }
    }

    private fun showAlertDialogStandard() {
        binding.btnShowAlertdialogstandar.setOnClickListener {
            val dialog = AlertDialog.Builder(requireContext())
            dialog.setTitle("Ini title dialog")
            dialog.setMessage("Ini deskripsi dialog")
            dialog.setCancelable(true)
            dialog.show()
        }
    }

}